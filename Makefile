SHELL := /bin/bash

.PHONY: install test lint clean clean-all clean-deps

install:
	pipenv install --dev

test:
	pipenv run pytest -vv --doctest-modules --doctest-continue-on-failure

lint:
	pipenv run mypy pvimport test
	pipenv run pylint pvimport test

clean:
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

clean-deps:
	find .venv/ -mindepth 1 -not -name '.gitkeep' -delete

clean-all: clean clean-deps