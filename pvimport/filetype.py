"""
List of RAW mime-types is based on:
https://github.com/angryziber/gnome-raw-thumbnailer/blob/932c008d41d34d19bdab30afa1b7318fbb75572c/data/raw-thumbnailer.xml
https://github.com/Beep6581/RawTherapee/blob/690fab3d08afcb2fba8504af1e2a8e1dd3564772/rtdata/mime-types
https://github.com/exiftool/exiftool/blob/74dbab1d2766d6422bb05b033ac6634bf8d1f582/lib/Image/ExifTool.pm#L216
"""
RAW_MIME_TYPES = {
    "image/x-adobe-dng",       # Adobe Digital Negative
    "image/x-canon-cr2",       # Canon
    "image/x-canon-cr3",       # Canon
    "image/x-canon-crw",       # Canon
    "image/x-dcraw",           # Digital Raw
    "image/x-epson-erf",       # Epson
    "image/x-fuji-raf",        # Fuji
    "image/x-fujifilm-raf",    # Fuji
    "image/x-gopro-gpr",       # GoPro
    "image/x-hasselblad-3fr",  # Hasselblad
    "image/x-hasselblad-fff",  # Hasselblad
    "image/x-kodak-dcr",       # Kodak
    "image/x-kodak-k25",       # Kodak
    "image/x-kodak-kdc",       # Kodak
    "image/x-leica-rwl",       # Leica
    "image/x-lif",             # Leica
    "image/x-mamiya-mef",      # Mamiya
    "image/x-minolta-mrw",     # Minolta
    "image/x-nikon-nef",       # Nikon
    "image/x-nikon-nrw",       # Nikon
    "image/x-olympus-orf",     # Olympus
    "image/x-panasonic-raw",   # Panasonic
    "image/x-panasonic-rw2",   # Panasonic
    "image/x-pentax-pef",      # Pentax
    "image/x-pentax-raw",      # Pentax
    "image/x-raw",             # PhaseOne (uses .TIF file extension)
    "image/x-rawzor",          # Rawzor
    "image/x-samsung-srw",     # Samsung
    "image/x-sigma-x3f",       # Sigma
    "image/x-sony-arw",        # Sony
    "image/x-sony-pmp",        # Sony
    "image/x-sony-sr2",        # Sony
    "image/x-sony-srf",        # Sony
    "image/x-zeiss-czi",       # Zeiss
}

"""
List of RAW file extensions is based on:
https://github.com/angryziber/gnome-raw-thumbnailer/blob/932c008d41d34d19bdab30afa1b7318fbb75572c/data/raw-thumbnailer.xml
"""
RAW_FILE_EXTENSIONS = [
    "dng",
    "crw",
    "cr2",
    "erf",
    "raf",
    "dcr",
    "k25",
    "kdc",
    "mrw",
    "nef",
    "orf",
    "raw",
    "pef",
    "x3f",
    "srf",
    "sr2",
    "arw"]
