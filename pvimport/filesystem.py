"""
Provides functions to operate on files
"""

import re
import os

from pathlib import Path

from typing import Iterable, List


class DirNotFoundError(Exception):
    """Directory not found error"""


def _get_dirname_key(dirname: str) -> List:
    """Gets sortable key for specified dirname. Examples:
    >>> _get_dirname_key("simple")
    [['simple'], 'simple']
    >>> _get_dirname_key("2021-01")
    [['2021', '01'], '2021-01']
    """
    dirname = dirname.casefold()
    parts = re.split("[^a-zA-Z0-9]", dirname)
    return [parts, dirname]


def _get_filename_key(filename: str) -> List:
    """Gets sortable key for specified filename. Examples:
    >>> _get_filename_key("simple.jpg")
    [['simple'], 'simple', ['jpg']]
    >>> _get_filename_key("20210928_180215_b76_1.jpg")
    [['20210928', '180215', 'b76', '1'], '20210928_180215_b76_1', ['jpg']]
    >>> _get_filename_key("20210928_180215_b76.nef.xmp")
    [['20210928', '180215', 'b76'], '20210928_180215_b76', ['nef', 'xmp']]
    """
    filename = filename.casefold()
    prefix, *extensions = filename.split(".")
    prefix_parts = re.split("[^a-zA-Z0-9]", prefix)
    return [prefix_parts, prefix, extensions]


def _is_hidden(path: str):
    """Check if file is hidden. Examples:
    >>> _is_hidden(".config")
    True
    >>> _is_hidden("data")
    False
    >>> _is_hidden("/home/user/.config")
    True
    >>> _is_hidden("/home/user/data")
    False
    """
    return os.path.basename(path).startswith(".")


def walk_directories(root_dir: str, ignore_hidden=True) -> Iterable[List[str]]:
    """Lists files recursively in batches per directory"""
    if not Path(root_dir).is_dir():
        raise DirNotFoundError(f'Directory "{root_dir}" does not exist')
    for dirpath, dirnames, filenames in os.walk(root_dir, followlinks=True):
        if ignore_hidden:
            dirnames[:] = [d for d in dirnames if not _is_hidden(d)]
        dirnames.sort(key=_get_dirname_key)
        if ignore_hidden:
            filenames[:] = [f for f in filenames if not _is_hidden(f)]
        if filenames:
            filenames.sort(key=_get_filename_key)
            yield [os.path.join(dirpath, f) for f in filenames]
