"""
Imports media into archive
"""

from pprint import pprint
from typing import List, Iterable

from pvimport import filesystem, exif, plan, relation, digest


def _get_source_file(main_path: str, main_exif: exif.FileExif, related_paths: List[str]):
    md5 = digest.calc_md5(main_path)
    return plan.SourceFile(
        path=main_path,
        related_paths=related_paths,
        mime_type=main_exif.mime_type,
        create_date=main_exif.create_date,
        digest=md5)


def _get_source_files(paths: Iterable[str], extensions: List[str]) -> Iterable[plan.SourceFile]:
    relations = relation.group_related(paths, extensions)
    for (main_path, related_paths) in relations.items():
        main_exif = exif.get_exif(main_path)
        yield _get_source_file(main_path, main_exif, related_paths)


def import_archive(
        source_dir: str,
        destination_dir: str,
        extensions: List[str],
        ignore_hidden=True) -> None:
    """Imports media from specified directory into destination directory
    applying renaming based on EXIF data and MD5 hash"""
    directories: Iterable[List[str]] = filesystem.walk_directories(
        source_dir, ignore_hidden=ignore_hidden)
    source_files: List[plan.SourceFile] = []
    for paths in directories:
        source_files.extend(_get_source_files(paths, extensions))
    operations: List[plan.FileOperation] = []
    for source_file in source_files:
        operations.extend(plan.create_plan(destination_dir, source_file))
    pprint(operations)
