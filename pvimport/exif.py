"""
Reads EXIF metadata from files.
"""

from pathlib import Path
from dataclasses import dataclass
from datetime import datetime
from typing import List, Iterable, Optional, Dict, Any

import exiftool  # type: ignore


ExifTags = Dict[str, Any]

PATH_TAG = "SourceFile"
MIME_TYPE_TAG = "MIMEType"
CREATE_DATE_TAGS = ["DateTimeOriginal", "CreateDate"]
ALL_TAGS = [PATH_TAG, MIME_TYPE_TAG, *CREATE_DATE_TAGS]

DATETIME_FORMAT = "%Y:%m:%d %H:%M:%S"
DATETIME_LENGTH = len("2000:01:01 00:00:00")
MIN_YEAR = 1971
MAX_YEAR = datetime.now().year + 1


@dataclass
class FileExif:
    """Stores EXIF information about the file"""
    path: str
    mime_type: Optional[str]
    create_date: Optional[datetime]


class ExifError(Exception):
    """EXIF processing error"""


def get_exif(filename: str) -> FileExif:
    """Read EXIF from a single file"""
    path = _try_resolve_path(filename)
    if path is None:
        raise ExifError(f'File "{filename}" does not exist')
    with exiftool.ExifToolHelper() as helper:
        tags = helper.get_tags([path], tags=ALL_TAGS)
        return _create_file_exif(tags[0])


def get_exif_batch(filenames: Iterable[str]) -> Iterable[FileExif]:
    """Read EXIF from multiple files"""
    paths = _try_resolve_paths(filenames)
    if paths:
        with exiftool.ExifToolHelper() as helper:
            tags_batch = helper.get_tags(paths, tags=ALL_TAGS)
            for tags in tags_batch:
                yield _create_file_exif(tags)


def _create_file_exif(tags: ExifTags) -> FileExif:
    create_date = _get_create_date_from_tags(tags)
    path = _get_path_from_tags(tags)
    mime_type = _get_mime_type_from_tags(tags)
    return FileExif(path=path, create_date=create_date, mime_type=mime_type)


def _get_path_from_tags(tags: ExifTags) -> str:
    if PATH_TAG not in tags:
        raise ExifError(f"Missing {PATH_TAG} tag")
    return str(tags[PATH_TAG])


def _get_create_date_from_tags(tags: ExifTags) -> Optional[datetime]:
    for tag_name in CREATE_DATE_TAGS:
        tag_values = _get_tag_values(tags, tag_name)
        for tag_value in tag_values:
            create_date = _try_parse_datetime(tag_value)
            if create_date is not None:
                return create_date
    return None


def _get_mime_type_from_tags(tags: ExifTags) -> Optional[str]:
    tag_values = _get_tag_values(tags, MIME_TYPE_TAG)
    for tag_value in tag_values:
        if tag_value:
            return str(tag_value)
    return None


def _get_tag_values(tags: ExifTags, tag_name: str) -> Iterable[str]:
    for (key, value) in tags.items():
        if key == tag_name or key.endswith(f":{tag_name}"):
            yield str(value).strip()


def _has_tag_with_wildcard(tags: ExifTags, tag_pattern: str) -> bool:
    begin_wildcard = False
    end_wildcard = False
    searched_tag = tag_pattern
    if tag_pattern.startswith("*"):
        begin_wildcard = True
        searched_tag = tag_pattern[1:]
    if tag_pattern.endswith("*"):
        end_wildcard = True
        searched_tag = tag_pattern[0:-1]
    for tag in tags:
        if _check_tag_name_matches(tag, searched_tag, begin_wildcard, end_wildcard):
            return True
    return False


def _check_tag_name_matches(
        tag: str,
        pattern: str,
        begin_wildcard: bool,
        end_wildcard: bool) -> bool:
    if begin_wildcard and end_wildcard:
        return pattern in tag
    if begin_wildcard:
        return tag.endswith(pattern)
    if end_wildcard:
        return tag.startswith(pattern) or f":{pattern}" in tag
    return tag == pattern or tag.endswith(f":{pattern}")


def _try_parse_datetime(value: str) -> Optional[datetime]:
    if len(value) < DATETIME_LENGTH:
        return None
    try:
        dt = datetime.strptime(value[0:DATETIME_LENGTH], DATETIME_FORMAT)
        if dt.year >= MIN_YEAR and dt.year <= MAX_YEAR:
            return dt
    except ValueError:
        pass
    return None


def _try_resolve_paths(filenames: Iterable[str]) -> List[str]:
    return [
        path for path
        in (_try_resolve_path(filename) for filename in filenames)
        if path is not None]


def _try_resolve_path(filename: str) -> Optional[str]:
    path = Path(filename)
    try:
        path = path.resolve(strict=True)
        if path.is_file():
            return str(path)
    except (FileNotFoundError, RuntimeError):
        pass
    return None
