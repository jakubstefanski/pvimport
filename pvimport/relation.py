"""
Finds relation between files based on filenames and priority of extensions
"""

import re
from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Tuple, Iterable, List, Dict


def _split_path(filename: str) -> Tuple[List[str], str, str]:
    """Gets filename without path and suffixes. Examples:
    >>> _split_path("DSC_6102.JPG")
    ([], 'DSC_6102', '.JPG')
    >>> _split_path("DSC_6103.NEF.XMP")
    ([], 'DSC_6103', '.NEF.XMP')
    >>> _split_path("dir/DSC_6104.JPG.XMP")
    (['dir'], 'DSC_6104', '.JPG.XMP')
    >>> _split_path("dir/path/DSC_6104.JPG")
    (['dir', 'path'], 'DSC_6104', '.JPG')
    """
    path = Path(filename)
    suffix = "".join(path.suffixes)
    basename = path.name.removesuffix(suffix)
    parents = [x.name for x in reversed(path.parents) if x.name]
    return (parents, basename, suffix)


def _is_related(filename: str, test: str) -> bool:
    """Checks if test filename is related to the filename
    >>> _is_related("test.jpg", "")
    False
    >>> _is_related("test.jpg", "test.jpg")
    False
    >>> _is_related("test.jpg", "test.xmp")
    True
    >>> _is_related("test.jpg", "test.jpg.xmp")
    True
    >>> _is_related("test.jpg", "test-edit.jpg")
    True
    >>> _is_related("test.jpg", "test-edit.jpg.xmp")
    True
    >>> _is_related("DSC_300.jpg", "DSC_3000.jpg")
    False
    >>> _is_related("a/b/test.jpg", "d/e/test.jpg")
    True
    >>> _is_related("a/b/test.jpg", "d/e/test-edit.jpg.xmp")
    True
    >>> _is_related("a/b/test.jpg", "a/b/qwerty.jpg")
    False
    """
    if test.casefold() == filename.casefold():
        return False  # same file
    (_, basename, _) = _split_path(filename)
    (_, test_basename, _) = _split_path(test)
    if test_basename.casefold() == basename.casefold():
        return True  # same basename but different extensions
    if re.match(f"^{re.escape(basename)}[^a-zA-Z0-9]", test_basename):
        return True  # extended with another word (e.g. "-edit" suffix)
    return False


def _matches_extension(filename: str, extension: str) -> bool:
    """Checks if two extensions matches. Examples:
    >>> _matches_extension("test.jpg", ".jpg")
    True
    >>> _matches_extension("test.jpeg", ".jpg")
    False
    >>> _matches_extension("test.png", ".jpg")
    False
    >>> _matches_extension("test.jpg", ".JPG")
    True
    >>> _matches_extension("test.JPG", "jpg")
    True
    >>> _matches_extension("testJPG", "JPG")
    False
    """
    return filename.casefold().endswith(f".{extension.lstrip('.').casefold()}")


def _get_relation_key(filename: str) -> List:
    """Gets sortable key for relations. Examples:
    >>> _get_relation_key("test.jpg")
    ['test', '.jpg', 'test.jpg']
    >>> _get_relation_key("TEST.JPG.XMP")
    ['test', '.jpg.xmp', 'TEST.JPG.XMP']
    >>> _get_relation_key("a/b/test.jpg")
    ['test', '.jpg', 'a/b/test.jpg']
    """
    (_, basename, suffix) = _split_path(filename)
    return [basename.casefold(), suffix.casefold(), filename]


@dataclass
class RelatedFile:
    path: str
    suffix: str
    extension: str
    subdirectories: List[str]


def _check_relation(main_path: str, test_path: str) -> Optional[RelatedFile]:
    """Checks if test_path is related to the main_path
    >>> _check_relation("test.jpg", "") is None  # empty test_path
    True
    >>> _check_relation("test.jpg", "different.jpg") is None  # different basename
    True
    >>> _check_relation("test.jpg", "test.jpg") is None  # same file
    True
    >>> _check_relation("subdir/test.nef", "test.jpg") is None  # parent dir
    True
    >>> _check_relation("one/two/test.nef", "one/test.jpg") is None  # parent deep dir
    True
    >>> _check_relation("one/test.nef", "two/test.jpg") is None  # sibling dir
    True
    >>> _check_relation("one/two/test.nef", "one/three/test.jpg") is None  # sibling deep dir
    True
    >>> _check_relation("test.jpg", "test.xmp")  # same basename other extension
    RelatedFile(path='test.xmp', suffix='', extension='.xmp', subdirectories=[])
    >>> _check_relation("test.jpg", "test-edit.jpg")  # same basename with suffix and same extension
    RelatedFile(path='test-edit.jpg', suffix='-edit', extension='.jpg', subdirectories=[])
    >>> _check_relation("test.nef", "test-edit.jpg")  # same basename other suffix and extension
    RelatedFile(path='test-edit.jpg', suffix='-edit', extension='.jpg', subdirectories=[])
    >>> _check_relation("test.jpg", "subdir/test.jpg")  # identical except in subdirectory
    RelatedFile(path='subdir/test.jpg', suffix='', extension='.jpg', subdirectories=['subdir'])
    >>> _check_relation("test.nef", "exported/edits/test-edit.jpg")  # RAW and JPG in subdirectory
    RelatedFile(path='exported/edits/test-edit.jpg', suffix='-edit', extension='.jpg', subdirectories=['exported', 'edits'])
    >>> _check_relation("images/test.nef", "images/test.xmp")  # RAW and XMP in same directory
    RelatedFile(path='images/test.xmp', suffix='', extension='.xmp', subdirectories=[])
    >>> _check_relation("common/test.jpg", "common/exported/test.jpg")  # common subdirectory
    RelatedFile(path='common/exported/test.jpg', suffix='', extension='.jpg', subdirectories=['exported'])
    """
    if test_path.casefold() == main_path.casefold():
        return None  # same path
    (main_subdirectories, main_basename, _) = _split_path(main_path)
    (test_subdirectories, test_basename, test_extension) = _split_path(test_path)
    if not test_basename.startswith(main_basename):
        return None  # different base name
    if len(main_subdirectories) > len(test_subdirectories):
        return None  # main_path is deeper than test_path
    subdirectories = test_subdirectories[:]
    for (main_dir, test_dir) in zip(main_subdirectories, test_subdirectories):
        if main_dir.casefold() == test_dir.casefold():
            subdirectories.remove(test_dir)  # remove common parents
        else:
            return None  # different subdirectory
    return RelatedFile(
        path=test_path,
        suffix=test_basename.removeprefix(main_basename),
        extension=test_extension,
        subdirectories=subdirectories)


def group_related(filenames: Iterable[str], extensions: List[str]) -> Dict[str, List[str]]:
    """Groups related filenames considering extensions as priority list"""
    # sorted by basename with preference for short names
    sorted_filenames = sorted(filenames, key=_get_relation_key)
    relations: Dict[str, List[str]] = {}
    # finds base files and creates empty relations
    for extension in extensions:
        for filename in sorted_filenames:
            if _matches_extension(filename, extension):
                for key in relations:
                    if _is_related(key, filename):
                        break
                else:  # adds new relation unless related to existing one
                    relations[filename] = []
    # fills relations found in the previous loop
    while sorted_filenames:
        filename = sorted_filenames.pop(0)
        for (key, values) in relations.items():
            if _is_related(key, filename):
                values.append(filename)
                break
    return relations
