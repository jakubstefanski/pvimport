#!/usr/bin/env python3

"""
Executable module for pvimport tool.
"""


import sys
import os

from pvimport import importer


def main():
    """Main entrypoint function"""
    args = sys.argv[1:]
    extensions = [a for a in args.pop(0).split(",") if a] if args else []
    if extensions:
        destination_dir = args.pop(0) if args else "."
        source_dir = args.pop(0) if args else os.getcwd()
        importer.import_archive(source_dir, destination_dir, extensions)
    else:
        print("Invalid arguments")  # TODO: use a library for options parsing


if __name__ == "__main__":
    main()
