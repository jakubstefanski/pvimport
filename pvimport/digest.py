"""
Calculates cryptographic digest of specified files
"""

import hashlib
from typing import IO

CHUNK_SIZE = 8 * 1024


def calc_md5(filename: str) -> str:
    """Calculates MD5 hash of a specified file"""
    with open(filename, "rb") as file:
        return _calc_md5_for_file(file)


def _calc_md5_for_file(file: IO) -> str:
    file_hash = hashlib.md5()
    while True:
        chunk = file.read(CHUNK_SIZE)
        if not chunk:
            break
        file_hash.update(chunk)
    return file_hash.hexdigest()
