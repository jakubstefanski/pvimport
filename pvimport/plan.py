"""
Provides functions to create import strategies
"""

import os.path
from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from typing import List, Iterable, Optional


@dataclass
class SourceFile:
    """Describes file to be imported"""
    path: str
    related_paths: List[str] = field(default_factory=list)
    create_date: Optional[datetime] = None
    digest: Optional[str] = None
    mime_type: Optional[str] = None


class OperationType(Enum):
    """Enum defining file operation type within plan"""
    COPY = "COPY"
    IGNORE = "IGNORE"


@dataclass
class FileOperation:
    """Describes file operation for a single file"""
    operation_type: OperationType
    source_path: str
    destination_path: str
    comment: str

    @classmethod
    def copy(cls, source_path: str, destination_path: str):
        """Creates FileOperation for moved files"""
        return FileOperation(
            source_path=source_path,
            operation_type=OperationType.COPY,
            destination_path=destination_path,
            comment="")

    @classmethod
    def ignore(cls, source_path: str, comment: str):
        """Creates FileOperation for ignored files"""
        return FileOperation(
            source_path=source_path,
            operation_type=OperationType.IGNORE,
            destination_path="",
            comment=comment)


class SourceFileError(Exception):
    """Source file error"""
    file: SourceFile

    def __init__(self, file: SourceFile, *args) -> None:
        super().__init__(*args)
        self.file = file


def create_plan(destination_dir: str, file: SourceFile) -> Iterable[FileOperation]:
    """Gets destination path for given file"""
    if not file.create_date:
        raise SourceFileError(file, "File is missing create date")
    if not file.digest:
        raise SourceFileError(file, "File is missing digest")
    digest = file.digest[0:8]
    _, extension = os.path.splitext(file.path)
    dir_parts = [str(file.create_date.year),
                 file.create_date.strftime("%Y-%m")]
    filename = f'{file.create_date.strftime("%Y%m%d_%H%M%S")}_{digest.lower()}{extension.lower()}'
    yield FileOperation.copy(file.path, os.path.join(destination_dir, *dir_parts, filename))


# def create_plan(destination_dir: str, files: Iterable[SourceFile]) -> List[FileOperation]:
#     """Creates import plan and returns list of file operations"""
#     operations: List[FileOperation] = []
#     for file in files:
#         try:
#             destination_path = Path(
#                 destination_dir, get_destination_path(file))
#         except SourceFileError as error:
#             operations.append(FileOperation.ignore(file, comment=str(error)))
#         else:
#             if destination_path.is_file():
#                 operations.append(FileOperation.ignore(
#                     file, "File already exists"))
#             else:
#                 operations.append(FileOperation.copy(
#                     file, str(destination_path)))
#     return operations
