"""
Tests filesystem module
"""

import test.data.files as f

import pytest

from pvimport.filesystem import DirNotFoundError, walk_directories


# pylint: disable=missing-function-docstring


def test_walk_directories_when_dir_not_exists_should_raise_error():
    with pytest.raises(DirNotFoundError):
        list(walk_directories(f.NOT_EXISTS))


def test_walk_directories_when_flat_dir_should_succeed():
    dir_files = list(walk_directories(f.FILESYSTEM_A_DIR))
    assert dir_files == [f.FILESYSTEM_A_FILES]


def test_walk_directories_when_deep_dir_and_ignore_hidden_should_returns_files_in_correct_order():
    dir_files = list(walk_directories(f.FILESYSTEM_DIR, ignore_hidden=True))
    assert dir_files == [f.FILESYSTEM_ROOT_FILES,
                         f.FILESYSTEM_A_FILES, f.FILESYSTEM_B_FILES]


def test_walk_directories_when_deep_dir_and_include_hidden_should_returns_files_in_correct_order():
    dir_files = list(walk_directories(f.FILESYSTEM_DIR, ignore_hidden=False))
    assert dir_files == [f.FILESYSTEM_ROOT_FILES, f.FILESYSTEM_DOT_X_FILES,
                         f.FILESYSTEM_A_FILES, f.FILESYSTEM_DOT_Y_FILES, f.FILESYSTEM_B_FILES]


if __name__ == "__main__":
    pytest.main([__file__, "-vv"])
