"""
Test data files
"""

from os.path import join
from typing import List, Tuple, Callable


NOT_EXISTS = "not_exists"


DIR_EXIF = join("test", "data", "exif")
EXIF_EMPTY = join(DIR_EXIF, "empty.jpg")
EXIF_HAS_CREATE_DATE = join(DIR_EXIF, "has_create_date.jpg")
EXIF_HAS_DATE_TIME_ORIGINAL = join(DIR_EXIF, "has_date_time_original.jpg")
EXIF_HAS_DATE_TIME_ORIGINAL_AND_CREATE_DATE = \
    join(DIR_EXIF, "has_date_time_original_and_create_date.jpg")


DIR_MD5 = join("test", "data", "md5")
MD5_1 = join(DIR_MD5, "1.jpg")
MD5_2 = join(DIR_MD5, "2.jpg")
MD5_3 = join(DIR_MD5, "3.jpg")
MD5_4 = join(DIR_MD5, "4.jpg")


def _create_file_registry() -> Tuple[List[str], Callable]:
    registry = []

    def _register_file(path: str) -> str:
        registry.append(path)
        return path
    return (registry, _register_file)


FILESYSTEM_DIR = join("test", "data", "filesystem")
FILESYSTEM_ROOT_FILES, _register_filesystem_root = _create_file_registry()
FILESYSTEM_ROOT_1 = _register_filesystem_root(join(FILESYSTEM_DIR, "1.jpg"))
FILESYSTEM_ROOT_2 = _register_filesystem_root(join(FILESYSTEM_DIR, "2.jpg"))

FILESYSTEM_A_DIR = join(FILESYSTEM_DIR, "a")
FILESYSTEM_A_FILES, _register_filesystem_a = _create_file_registry()
FILESYSTEM_A_3 = _register_filesystem_a(join(FILESYSTEM_A_DIR, "3.jpg"))
FILESYSTEM_A_4 = _register_filesystem_a(join(FILESYSTEM_A_DIR, "4.jpg"))
FILESYSTEM_A_5 = _register_filesystem_a(join(FILESYSTEM_A_DIR, "5.jpg"))

FILESYSTEM_B_DIR = join(FILESYSTEM_DIR, "b")
FILESYSTEM_B_FILES, _register_filesystem_b = _create_file_registry()
FILESYSTEM_B_6 = _register_filesystem_b(join(FILESYSTEM_B_DIR, "6.jpg"))
FILESYSTEM_B_6_XMP = _register_filesystem_b(
    join(FILESYSTEM_B_DIR, "6.jpg.xmp"))
FILESYSTEM_B_6_A = _register_filesystem_b(join(FILESYSTEM_B_DIR, "6_a.jpg"))
FILESYSTEM_B_6_A_XMP = _register_filesystem_b(
    join(FILESYSTEM_B_DIR, "6_a.jpg.xmp"))
FILESYSTEM_B_6_B = _register_filesystem_b(join(FILESYSTEM_B_DIR, "6-b.jpg"))
FILESYSTEM_B_6_B_XMP = _register_filesystem_b(
    join(FILESYSTEM_B_DIR, "6-b.jpg.xmp"))

FILESYSTEM_DOT_X_DIR = join(FILESYSTEM_DIR, ".x")
FILESYSTEM_DOT_X_FILES, _register_filesystem_dot_x = _create_file_registry()
FILESYSTEM_DOT_X_7 = _register_filesystem_dot_x(
    join(FILESYSTEM_DOT_X_DIR, "7.jpg"))
FILESYSTEM_DOT_X_8 = _register_filesystem_dot_x(
    join(FILESYSTEM_DOT_X_DIR, "8.jpg"))

FILESYSTEM_DOT_Y_DIR = join(FILESYSTEM_A_DIR, ".y")
FILESYSTEM_DOT_Y_FILES, _register_filesystem_dot_y = _create_file_registry()
FILESYSTEM_DOT_Y_9 = _register_filesystem_dot_y(
    join(FILESYSTEM_DOT_Y_DIR, "9.jpg"))
