"""
Tests hash module
"""

import test.data.files as f

import pytest

from pvimport.digest import calc_md5


# pylint: disable=missing-function-docstring


def test_calc_md5_when_file_exists_should_return_correct_md5():
    assert calc_md5(
        f.EXIF_EMPTY) == "67a4b3b7d0cc4b8bfe577ee329af8088"
    assert calc_md5(
        f.EXIF_HAS_DATE_TIME_ORIGINAL) == "34f32489f0c875593a9e76c654cb3296"
    assert calc_md5(
        f.EXIF_HAS_CREATE_DATE) == "686f4db7b107daaba75a50890cfcc177"
    assert calc_md5(
        f.EXIF_HAS_DATE_TIME_ORIGINAL_AND_CREATE_DATE), "170804a4be8a39421e74222fc972df8a"


def test_calc_md5_when_file_not_exists_should_raise_error():
    with pytest.raises(FileNotFoundError):
        calc_md5(f.NOT_EXISTS)


if __name__ == "__main__":
    pytest.main([__file__, "-vv"])
