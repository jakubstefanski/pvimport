"""
Tests plan module
"""

from datetime import datetime
from os.path import join

import pytest

from pvimport.plan import create_plan, SourceFile, FileOperation, SourceFileError


# pylint: disable=missing-function-docstring,line-too-long,use-implicit-booleaness-not-comparison


DESTINATION_DIR = "/home/user/Pictures"


def _create_jpg(path: str, create_date_str: str = "", **kwargs) -> SourceFile:
    create_date = datetime.fromisoformat(
        create_date_str) if create_date_str else None
    return SourceFile(
        path=path,
        mime_type="image/jpg",
        create_date=create_date,
        **kwargs)


def test_create_plan_when_file_is_missing_digest_should_raise_error():
    file = _create_jpg("1.jpg", "2021-03-01T10:25:11")
    with pytest.raises(SourceFileError):
        list(create_plan(DESTINATION_DIR, file))


def test_create_plan_when_file_is_missing_create_date_should_raise_error():
    file = _create_jpg("1.jpg", None, digest="hash123456")
    with pytest.raises(SourceFileError):
        list(create_plan(DESTINATION_DIR, file))


def test_create_plan_when_valid_jpg_should_succeed():
    file = _create_jpg("1.jpg", "2021-03-01T10:25:11", digest="hash123456")
    operations = list(create_plan(DESTINATION_DIR, file))
    assert operations == [FileOperation.copy(file.path, join(
        DESTINATION_DIR, "2021", "2021-03", "20210301_102511_hash1234.jpg"))]


# def test_get_destination_path_when_uppercase_extension_should_convert_lowercase():
#     file = _create_jpg("1.JPG", "2021-03-01T10:25:11", digest="hash123456")
#     destination_path = get_destination_path(file)
#     assert destination_path == join(
#         "2021", "2021-03", "20210301_102511_hash1234.jpg")


# def test_get_destination_path_when_no_extension_should_succeed():
#     file = _create_jpg("test", "2021-03-01T10:25:11", digest="hash123456")
#     destination_path = get_destination_path(file)
#     assert destination_path == join(
#         "2021", "2021-03", "20210301_102511_hash1234")


# def test_create_plan_when_no_source_files_should_succeed():
#     operations = create_plan(DESTINATION_DIR, [])
#     assert operations == []


# def test_create_plan_when_single_correct_file_should_succeed():
#     file = _create_jpg("1.jpg", "2021-03-01T10:25:11", digest="hash123456")
#     operations = create_plan(DESTINATION_DIR, [file])
#     assert operations == [
#         FileOperation.copy(file, join(DESTINATION_DIR, "2021", "2021-03", "20210301_102511_hash1234.jpg"))]


# def test_create_plan_when_extension_uppercase_should_convert_lowercase():
#     file = _create_jpg("1.JPG", "2021-03-01T10:25:11", digest="hash123456")
#     operations = create_plan(DESTINATION_DIR, [file])
#     assert operations == [
#         FileOperation.copy(file, join(DESTINATION_DIR, "2021", "2021-03", "20210301_102511_hash1234.jpg"))]


# def test_create_plan_when_no_extension_should_succeed():
#     file = _create_jpg("test", "2021-03-01T10:25:11", digest="hash123456")
#     operations = create_plan(DESTINATION_DIR, [file])
#     assert operations == [
#         FileOperation.copy(file, join(DESTINATION_DIR, "2021", "2021-03", "20210301_102511_hash1234"))]


# def test_create_plan_when_multiple_correct_files_should_succeed():
#     files = [
#         _create_jpg("1.jpg", "2021-03-01T10:25:11", digest="hash123456"),
#         _create_jpg("2.jpg", "2021-04-22T11:19:26", digest="hashQWERTY"),
#         _create_jpg("3.jpg", "2022-08-30T07:42:45", digest="hash987654")]
#     operations = create_plan(DESTINATION_DIR, files)
#     assert operations == [FileOperation.copy(f, join(
#         DESTINATION_DIR,
#         f"{f.create_date.year:04}", f"{f.create_date.year:04}-{f.create_date.month:02}",
#         f"{f.create_date.year:04}{f.create_date.month:02}{f.create_date.day:02}_{f.create_date.hour:02}{f.create_date.minute:02}{f.create_date.second:02}_{f.digest[:8]}.jpg"))
#         for f in files]


# def test_create_plan_when_single_file_with_missing_digest_should_ignore_file():
#     file = _create_jpg("1.jpg", "2021-03-01T10:25:11", digest=None)
#     operations = create_plan(DESTINATION_DIR, [file])
#     assert operations == [
#         FileOperation.ignore(file, "File is missing digest")]


# def test_create_plan_when_multiple_files_some_incorrect_should_succeed():
#     files = [
#         _create_jpg("1.jpg", "2021-03-01T10:25:11", digest="hash123456"),
#         _create_jpg("2.jpg", digest="hash123456"),
#         _create_jpg("3.jpg", "2021-02-11T11:32:27", digest=None),
#         _create_jpg("4.jpg"),
#         _create_jpg("5.jpg", "2022-09-21T21:02:20", digest="hash987654")]
#     operations = create_plan(DESTINATION_DIR, files)
#     assert operations == [
#         FileOperation.copy(files[0], join(
#             DESTINATION_DIR, "2021", "2021-03", "20210301_102511_hash1234.jpg")),
#         FileOperation.ignore(files[1], "File is missing create date"),
#         FileOperation.ignore(files[2], "File is missing digest"),
#         FileOperation.ignore(files[3], "File is missing create date"),
#         FileOperation.copy(files[4], join(DESTINATION_DIR, "2022", "2022-09", "20220921_210220_hash9876.jpg"))]


if __name__ == "__main__":
    pytest.main([__file__, "-vv"])
