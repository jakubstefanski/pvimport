"""
Tests relation module
"""

import pytest

from pvimport.relation import group_related


# pylint: disable=missing-function-docstring,line-too-long,use-implicit-booleaness-not-comparison

def test_group_related_when_empty_filenames_should_return_empty():
    relations = group_related([], ["jpg"])
    assert relations == {}


def test_group_related_when_single_filename_should_succeed():
    relations = group_related(["test.jpg"], ["jpg"])
    assert relations == {"test.jpg": []}


def test_group_related_when_single_related_file_via_extension_should_succeed():
    relations = group_related(["test.jpg", "test.xmp"], ["jpg"])
    assert relations == {"test.jpg": ["test.xmp"]}


def test_group_related_when_single_related_file_via_suffix_should_succeed():
    relations = group_related(["test.jpg", "test.jpg.xmp"], ["jpg"])
    assert relations == {"test.jpg": ["test.jpg.xmp"]}


def test_group_related_when_unrelated_file_should_return_empty():
    relations = group_related(["test.png"], ["jpg"])
    assert relations == {}


def test_group_related_when_unrelated_file_should_return_only_related():
    relations = group_related(
        ["trash.bin", "test-edit.jpg", "test.jpg.xmp", "test.jpg", "README.txt"], ["jpg"])
    assert relations == {"test.jpg": ["test.jpg.xmp", "test-edit.jpg"]}


def test_group_related_when_two_extensions_should_respect_priority():
    relations = group_related(
        ["test-edit.jpg", "test.xmp", "test.jpg", "test.nef"],
        ["nef", "jpg"])
    assert relations == {"test.nef": ["test.jpg", "test.xmp", "test-edit.jpg"]}


def test_group_related_when_multiple_extensions_and_multiple_groups_should_succeed():
    relations = group_related(
        ["test-edit.jpg", "test.xmp", "test.jpg", "another.jpg", "another.nef"],
        ["nef", "jpg"])
    assert relations == {
        "test.jpg": ["test.xmp", "test-edit.jpg"],
        "another.nef": ["another.jpg"]}


def test_group_related_various_files_should_succeed():
    relations = group_related(
        ["trash.bin", "test-edit.jpg", "another.xmp", "test.jpg.xmp", "raw.jpg",
         "test-edit.jpg.xmp", "another.jpg", "test.jpg", "more.png", "raw.nef", "raw.xmp"],
        ["nef", "jpg", "png"])
    assert relations == {
        "raw.nef": ["raw.jpg", "raw.xmp"],
        "another.jpg": ["another.xmp"],
        "test.jpg": ["test.jpg.xmp", "test-edit.jpg", "test-edit.jpg.xmp"],
        "more.png": []}


def test_group_related_when_subdirectories_should_succeed():
    relations = group_related(["metadata/test.xmp",  "test-edit.jpg", "test.jpg"], ["jpg"])
    assert relations == {"test.jpg": ["metadata/test.xmp", "test-edit.jpg"]}


if __name__ == "__main__":
    pytest.main([__file__, "-vv"])
