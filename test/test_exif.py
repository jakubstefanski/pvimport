"""
Tests exif module
"""

import os
from datetime import datetime

import test.data.files as f

import pytest

from pvimport import exif


# pylint: disable=missing-function-docstring,line-too-long


def _abs(filename: str) -> str:
    return os.path.join(os.getcwd(), filename)


def test_get_exif_when_file_not_exists_should_raise_error():
    with pytest.raises(exif.ExifError):
        exif.get_exif(f.NOT_EXISTS)


def test_get_exif_batch_when_file_not_exists_should_skip():
    assert len(list(exif.get_exif_batch([f.NOT_EXISTS]))) == 0
    assert len(list(exif.get_exif_batch([f.NOT_EXISTS, f.NOT_EXISTS]))) == 0
    assert len(list(exif.get_exif_batch([f.EXIF_EMPTY, f.NOT_EXISTS]))) == 1


def test_get_exif_when_file_has_no_exif_should_succeed():
    file = exif.get_exif(f.EXIF_EMPTY)
    assert file.path == _abs(f.EXIF_EMPTY)
    assert file.create_date is None


def test_get_exif_batch_when_file_has_no_exif_should_succeed():
    files = exif.get_exif_batch([f.EXIF_EMPTY, f.EXIF_EMPTY])
    assert len(list(files)) == 2
    for file in files:
        assert file.path == _abs(f.EXIF_EMPTY)
        assert file.create_date is None


def test_get_exif_when_file_has_date_time_original_should_succeed():
    file = exif.get_exif(f.EXIF_HAS_DATE_TIME_ORIGINAL)
    assert file.path == _abs(f.EXIF_HAS_DATE_TIME_ORIGINAL)
    assert file.create_date == datetime(2021, 12, 26, 0, 24, 11)


def test_get_exif_when_file_has_create_date_should_succeed():
    file = exif.get_exif(f.EXIF_HAS_CREATE_DATE)
    assert file.path == _abs(f.EXIF_HAS_CREATE_DATE)
    assert file.create_date == datetime(2021, 12, 26, 0, 18, 0)


def test_get_exif_when_file_has_date_time_original_and_create_date_should_prefer_date_time_original():
    file = exif.get_exif(f.EXIF_HAS_DATE_TIME_ORIGINAL_AND_CREATE_DATE)
    assert file.path == _abs(f.EXIF_HAS_DATE_TIME_ORIGINAL_AND_CREATE_DATE)
    assert file.create_date == datetime(2021, 12, 26, 0, 24, 11)


def test_get_exif_batch_when_multiple_files_should_succeed():
    has_date_time_original, has_create_date = exif.get_exif_batch([f.EXIF_HAS_DATE_TIME_ORIGINAL, f.EXIF_HAS_CREATE_DATE])
    assert has_date_time_original.path == _abs(f.EXIF_HAS_DATE_TIME_ORIGINAL)
    assert has_date_time_original.create_date == datetime(2021, 12, 26, 0, 24, 11)
    assert has_create_date.path == _abs(f.EXIF_HAS_CREATE_DATE)
    assert has_create_date.create_date == datetime(2021, 12, 26, 0, 18, 0)


if __name__ == "__main__":
    pytest.main([__file__, "-vv"])
